﻿using System;
using System.Linq;
using System.Collections.Generic;
using MangaLibrary;

class Program
{
    static void Main()
    {
        //Liste mit Manga-Objekten erstellen
        List<Manga> mangaList = new List<Manga>();

        //Beispiel-Mangas hinzufügen
        mangaList.Add(new Manga("One Piece", "Eiichiro Oda", "Ruffy, Luffy", "Eiichiro Oda", "Shueisha", 100, "Adventure, Piraten"));
        mangaList.Add(new Manga("Naruto", "Masashi Kishimoto", "Naruto, Uzumaki", "Masashi Kishimoto", "Shueisha", 2, "Adventure, Ninja"));
        mangaList.Add(new Manga("Death Note", "Tsugumi Ohba", "Light Yagami", "Takeshi Obata", "Shueisha", 12, "Mystery, Detektiv, Actiondrama"));

        //Benutzer nach Suchkriterien fragen
        Console.WriteLine("Welches Kriterium möchten Sie verwenden, um nach Mangas zu suchen?");
        Console.WriteLine("1. Titel");
        Console.WriteLine("2. Autor");
        Console.WriteLine("3. MainCharacter");
        Console.WriteLine("4. Verlag");
        Console.WriteLine("5. Genre");
        int searchCriteria = int.Parse(Console.ReadLine());

        //Benutzer nach Suchbegriff fragen
        Console.WriteLine("Bitte geben Sie den Suchbegriff ein:");
        string searchTerm = Console.ReadLine();

        //Mangas suchen  (ToLower Befehl wird genutzt um verschiedene Schreibweisen aufzunehmen)
        var result = new List<Manga>(); //Mit dem Schlüsselwort var können Sie eine lokale Variable (innerhalb einer Methode oder einer Schleife) deklarieren, ohne den Typ explizit anzugeben
        switch (searchCriteria)
        {
            case 1:
                result = mangaList.Where(m => m.Title.ToLower().Contains(searchTerm.ToLower())).ToList();
                break;
            case 2:
                result = mangaList.Where(m => m.Author.ToLower().Contains(searchTerm.ToLower())).ToList();
                break;
            case 3:
                result = mangaList.Where(m => m.MainCharacter.ToLower().Contains(searchTerm.ToLower())).ToList();
                break;
            case 4:
                result = mangaList.Where(m => m.Publisher.ToLower().Contains(searchTerm.ToLower())).ToList();
                break;
            case 5:
                result = mangaList.Where(m => m.Genre.ToLower().Contains(searchTerm.ToLower())).ToList();
                break;
            default:
                Console.WriteLine("Ungültige Auswahl\nTreffen Sie eine Auswahl die gegeben ist");
                break;
        }

        //Ergebnisse anzeigen
        Console.WriteLine("Ergebnisse:");
        foreach (Manga manga in result)
        {
            Console.WriteLine(manga + " \n");

        }
        
        //Nimmt die Entertaste auf
        Console.ReadKey();

        //Setzt den ausgeführten Code zurück.
        Console.Clear();

        //Geht zurück zur Main Methode
        Main();
    }
}
